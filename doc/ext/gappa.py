from pygments.lexer import RegexLexer, words
from pygments.token import Text, Comment, Operator, Keyword, Name, Number, Punctuation, Error

class GappaLexer(RegexLexer):
    name = 'Gappa'
    aliases = 'gappa'

    keywords = (
        'not', 'in', 'float', 'fixed', 'int', 'sqrt', 'fma'
    )

    tokens = {
        'root': [
            (r'\s+', Text),
            (r'\#.*$', Comment.Singleline),
            (words(keywords, suffix=r'\b'), Keyword),

            (r'-?0[xX][\da-fA-F][\da-fA-F]*([.][\da-fA-F]*)?([pP][+-]?\d\d*)?', Number.Float),
            (r'-?\d\d*([.]\d*)?([bBeE][+-]?\d\d*)?', Number.Float),

            (r'(->|/\\|\\/)', Punctuation),
            (r'[-*+=<>/]', Operator),
            (r'[][|@{},;()?$~]', Punctuation),
            (r"[^\W\d]\w*", Name),
            (r"...", Keyword.Pseudo),
        ],
    }

from sphinx.highlighting import lexers

lexers['gappa'] = GappaLexer(startinline=True)

def setup(app):
    return {
        'version': '0.1',
        'parallel_read_safe': True,
        'parallel_write_safe': True,
    }
