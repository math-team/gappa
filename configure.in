AC_INIT([Gappa], [1.6.0],
        [Guillaume Melquiond <guillaume.melquiond@inria.fr>],
        [gappa])

AC_CONFIG_MACRO_DIR([m4])

AC_PROG_CXX
AC_PROG_LEX(noyywrap)
AC_PROG_YACC
AX_PROG_CXX_FOR_BUILD

AC_ARG_ENABLE([valgrind],
  AS_HELP_STRING([--enable-valgrind], [enable extra checks when run under valgrind]),
  [CPPFLAGS="$CPPFLAGS -DUSE_VALGRIND -DLEAK_CHECKER"])

AC_LANG(C++)

AC_CHECK_HEADER([gmp.h], ,
  AC_MSG_ERROR([ *** Unable to find the GMP library (https://gmplib.org/)]))
AC_CHECK_LIB(gmp, __gmpz_init, ,
  AC_MSG_ERROR([ *** Unable to find the GMP library (https://gmplib.org/)]))
AC_CHECK_HEADER([mpfr.h], ,
  AC_MSG_ERROR([ *** Unable to find the MPFR library (https://www.mpfr.org/)]))
AC_CHECK_LIB(mpfr, mpfr_snprintf, ,
  AC_MSG_ERROR([ *** Unable to find the MPFR library (https://www.mpfr.org/)]))
AC_CHECK_HEADER([boost/numeric/interval.hpp], ,
  AC_MSG_ERROR([ *** Unable to find the Boost library (https://www.boost.org/)]))

AC_ARG_VAR(REMAKE, [Remake [vendored version]])

if test -z "$REMAKE"; then
AC_MSG_NOTICE([building remake...])
case `uname -s` in
MINGW*)
	$CXX -Wall -O2 -o remake.exe remake.cpp -lws2_32
	if test $? != 0; then AC_MSG_FAILURE([failed]); fi
	REMAKE=./remake.exe
	;;
*)
	$CXX_FOR_BUILD -Wall -O2 -o remake remake.cpp
	if test $? != 0; then AC_MSG_FAILURE([failed]); fi
	REMAKE=./remake
	;;
esac
fi

AC_CONFIG_HEADERS(config.h)
AC_CONFIG_FILES(stamp-config_h, [touch stamp-config_h])
AC_CONFIG_FILES(Remakefile)
AC_OUTPUT
